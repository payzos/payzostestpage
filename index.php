<form action="" method="POST">
    <label>pay 0.001 $</label>
    <input name="submit" type="submit">
</form>

<?php

include __DIR__ . "/env.php";


function pay()
{
    $data = [
        "destination_hash" => "tz1a3T5mRR1XWM1JgydvAqxEwpNs7ktVZuXc",
        "total" => "0.001",
        "currency" => "USD",
        "back_url" => URL . "back_url.php"
    ];
    $url = PAYZOS . "api/make_payment";
    // var_dump($data);
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);

    $arr  = json_decode($response, true);
    return $arr['data']["url"];
}




if (isset($_POST["submit"])) {
    // var_dump(pay());
    // echo "<script>const url = '".pay()"'</script>";
    // header("Location: " . pay());
    // var_dump pay();
?>
    <script>
        const url = '<?= pay() ?>';
        if (typeof url !== 'undefined')
            window.location.replace(url);
    </script>
<?php
}

?>