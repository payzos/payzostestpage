<?php
include __DIR__ . "/env.php";

header('Accept: */*');
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');

echo json_encode([
    'redirect_url' => URL . "redirect.php"
]);
